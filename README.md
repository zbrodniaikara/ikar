# LICENSE
    Apache License - Version 2.0, January 2004
    http://www.apache.org/licenses/LICENSE-2.0.html

# TODO:

* PLAYER:
    *   input / sterowanie
    *   zliczanie obrotów z rolki myszki + GUI
    *   animacje ruchu

* spawnowanie terenu [endless run]
    *   przeszkody [kolizje], malusowe punkty
    *   znajdźki [kolizje], bonusowe punkty

* menusy: start gry, menu w grze [pauza], podsumowanie treningu, restart gry

* czas. Licznik czasu potrzebnego na pokonanie trasy. Po zakończeniu nadmiarowy czas jest dodawany do punktów.

* PUNKTY:
    *   kolizje -> efekty. Screenshake na przeszkodę, brokat na znajdźki
    *   dodawanie i odejmowanie punktów
    *   zamiana liczniku czasu na punkty

* predefiniowane treningi
    *   wczytywane ze źródła danych [plik, db]
    *   dane mapy
        *   layout mapy
        *   wymagany pułap/prędkość
        *   intensywność wychyleń [na jamie będzie w layoucie]
        *   długość trasy [na jamie będzie w layoucie]


# CREDITS:
* Zbrodnia Ikara Team 2017
    *    Jerzy Woźniak
    *    Szymon Filipowicz
    *    Klaudia Wiśniewska
    *    Stanislav Mishchenko
    *    Melchior Moroz
* Enclave Phaser Template